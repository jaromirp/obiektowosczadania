package pl.sda.obiektowosc.ulamki;

public class Main {
    public static void main(String[] args) {
        Ulamek pol = new Ulamek(1, 2);
        pol.wyswietlUlamek();

        System.out.println();

        Ulamek cwierc = pol.pomnoz(pol);
        cwierc.wyswietlUlamek();

        System.out.println();

        //Wynik z tego działa wykorzystuje do kolejnego działania Method Chain
        Ulamek osemka = pol.pomnoz(pol).pomnoz(pol);
        osemka.wyswietlUlamek();

        System.out.println();

        Ulamek jeden = pol.podziel(pol);
        jeden.wyswietlUlamek();

        System.out.println();

        Ulamek pol3 = cwierc.dodaj(cwierc);
        pol3.wyswietlUlamek();

        System.out.println();

        Ulamek zero = pol.odejmij(pol);
        zero.wyswietlUlamek();

    }

}

