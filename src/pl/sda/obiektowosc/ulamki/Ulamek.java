package pl.sda.obiektowosc.ulamki;

public class Ulamek {
    private int licznik;
    private int mianownik;

    public Ulamek(int licznik, int mianownik) {
        this.licznik = licznik;
        this.mianownik = mianownik;
    }

    public void wyswietlUlamek() {
        System.out.printf("%d/%d", licznik, mianownik);
    }


    public Ulamek pomnoz(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.licznik;
        int mianownik = this.mianownik * ulamek.mianownik;

        return new Ulamek(licznik, mianownik);

    }

    public Ulamek podziel(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik;
        int mianownik = this.mianownik * ulamek.licznik;

        return new Ulamek(licznik, mianownik);
    }

    public Ulamek dodaj(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik + ulamek.licznik * this.mianownik;
        int mianownik = this.mianownik * ulamek.mianownik;
        return new Ulamek(licznik, mianownik);
    }

    public Ulamek odejmij(Ulamek ulamek) {
        int licznik = this.licznik * ulamek.mianownik - ulamek.licznik * this.mianownik;
        int mianownik = this.mianownik * ulamek.mianownik;
        return new Ulamek(licznik, mianownik);
    }

}